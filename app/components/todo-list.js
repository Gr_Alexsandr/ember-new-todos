import Component from '@ember/component';

export default Component.extend({
  actions: {
    complete(todo) {
      this.sendAction('completed', todo);
    },

    deleteTodo(todo) {
      this.sendAction('deleteTodo', todo);
    },

    editTodo(id, title) {
      this.set('id', id);
      this.sendAction('editTodo', this.get('id'), title);
    },

    handleValue(event) {
      if (event.keyCode === 13) {
        const title = event.target.value;
        this.sendAction('editTodoTitle', this.get('id'), title);
        this.set('value',  title);
      }
    }
  }
});
