import Component from '@ember/component';

export default Component.extend({
  id: 0,

  actions: {
    handleValue(event) {
      if (event.keyCode === 13) {
        const id = this.get('id');
        this.sendAction('addTodo', {
          id,
          title: event.target.value,
          done: false,
          edit: false
        });
        this.set('id', this.get('id') + 1);
        event.target.value = '';
      }
    }
  }
});
