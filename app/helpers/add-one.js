import { helper } from '@ember/component/helper';

export function addOne(params/*, hash*/) {
  const plusOne = params[0] + 1 + '.';
  return plusOne;
}

export default helper(addOne);
