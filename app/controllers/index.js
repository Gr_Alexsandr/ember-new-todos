import Controller from '@ember/controller';

export default Controller.extend({
  todos: [],

  active: function () {
    return this.get('todos').filter(todo => !todo.done);
  }.property('todos'),

  completed: function () {
    return this.get('todos').filter(todo => todo.done);
  }.property('todos'),

  actions: {
    addTodo(todo) {
      const todos = this.get('todos').concat([todo]);
      this.set('todos', todos);
    },

    completed(todoCompleted) {
      const {id, title, edit, done} = todoCompleted;
      const index = this.get('todos').findIndex(todo => todo.id === todoCompleted.id);
      const todos = [...this.get('todos')];
      todos[index] = {id, title, edit, done: !done};
      this.set('todos', todos);
    },

    deleteTodo(id) {
      const todos = this.get('todos').filter(todo => todo.id !== id);
      this.set('todos', todos);
    },

    editTodo(id, title, done) {
      const index = this.get('todos').findIndex(todo => todo.id === id);
      const todos = [...this.get('todos')];
      todos[index] = {id, title, done, edit: true};
      this.set('todos', todos);
    },

    editTodoTitle(id, title, done) {
      const index = this.get('todos').findIndex(todo => todo.id === id);
      const todos = [...this.get('todos')];
      todos[index] = {id, title, done, edit: false};
      this.set('todos', todos);
    }
  }
});
